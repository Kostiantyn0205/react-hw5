import ListCard from "../components/ListCard;";
import PropTypes from "prop-types";
import FormikForm from "../components/Form";

function Basket(){
    return (
        <div className={"basket-container"}>
            <ListCard pageName="Basket"></ListCard>

            <FormikForm />
        </div>
    )
}

Basket.propTypes = {
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default Basket;