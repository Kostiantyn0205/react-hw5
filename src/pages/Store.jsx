import ListCard from "../components/ListCard;";
import PropTypes from "prop-types";

function Store() {
    return (
        <ListCard></ListCard>
    )
}

Store.propTypes = {
    racingCars: PropTypes.array,
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default Store;