import {updateCarAC} from "./carReducer";

const localStorageDataFavorites = JSON.parse(localStorage.getItem("counterFavorites"));

const initialState = {
    counterFavorites: localStorageDataFavorites || 0,
};

const UPDATE_FAVORITES = 'UPDATE_FAVORITES'
const UPDATE_FAVORITES_ADD = 'UPDATE_FAVORITES_ADD'
const UPDATE_FAVORITES_SUB = 'UPDATE_FAVORITES_SUB'

const favoritesReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_FAVORITES:
            return {
                ...state,
                counterFavorites: action.payload,
            };
        case UPDATE_FAVORITES_ADD:
            return {
                ...state,
                counterFavorites: state.counterFavorites + 1,
            };
        case UPDATE_FAVORITES_SUB:
            return {
                ...state,
                counterFavorites: state.counterFavorites - 1,
            };
        default:
            return state;
    }
};

const updateFavoritesAddAC = () => ({
    type: UPDATE_FAVORITES_ADD,
});
const updateFavoritesSubAC = () => ({
    type: UPDATE_FAVORITES_SUB,
});

export const updateFavoritesAC = (payload) => ({
    type: UPDATE_FAVORITES,
    payload: payload
});

export const toggleF = (id, cars, dispatch) => {
    const newRacingCars = [...cars];
    newRacingCars[id].completed = !newRacingCars[id].completed;
    newRacingCars[id].completed ? dispatch(updateFavoritesAddAC()) : dispatch(updateFavoritesSubAC());
    dispatch(updateCarAC(newRacingCars));
}

export default favoritesReducer;