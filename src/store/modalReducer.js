const initialState = {
    showModal: false,
    modals: {},
};

const TOGGLE_MODAL = 'TOGGLE_MODAL';

const modalReducer = (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_MODAL:
            return {
                ...state,
                modals: {
                    ...state.modals,
                    [action.cardId]: action.modalState,
                },
            };
        default:
            return state;
    }
};

export const toggle = (cardId, modalState) => ({
    type: "TOGGLE_MODAL",
    cardId,
    modalState,
});

export default modalReducer;