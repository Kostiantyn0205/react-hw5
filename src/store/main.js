import { composeWithDevTools } from '@redux-devtools/extension';
import { applyMiddleware, combineReducers, legacy_createStore as createStore } from 'redux';
import thunk from 'redux-thunk';
import modalReducer from "./modalReducer";
import selectedReducer from "./selectedReducer";
import favoritesReducer from "./favoritesReducer";
import carReducer from "./carReducer";

const rootReducer = combineReducers({
    modal: modalReducer,
    choose: selectedReducer,
    favorites: favoritesReducer,
    car: carReducer
});

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)));

export default store;