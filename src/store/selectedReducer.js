import {updateCarAC} from "./carReducer";

const localStorageDataSelected = JSON.parse(localStorage.getItem("counterSelected"));

const initialState = {
    counterSelected: localStorageDataSelected || 0
};

const UPDATE_SELECTED_ADD = 'UPDATE_SELECTED_ADD'
const UPDATE_SELECTED_SUB = 'UPDATE_SELECTED_SUB'
const UPDATE_SELECTED_REMOVE = 'UPDATE_SELECTED_REMOVE'

const selectedReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_SELECTED_ADD:
            return {
                ...state,
                counterSelected: state.counterSelected + 1,
            };
        case UPDATE_SELECTED_SUB:
            return {
                ...state,
                counterSelected: state.counterSelected - 1,
            };
        case UPDATE_SELECTED_REMOVE:
            return {
                ...state,
                counterSelected: action.payload,
            };
        default:
            return state;
    }
};

const updateSelectedAddAC = () => ({
    type: UPDATE_SELECTED_ADD,
});
const updateSelectedSubAC = () => ({
    type: UPDATE_SELECTED_SUB,
});

export const updateSelectedRemoveAC = (payload) => ({
    type: UPDATE_SELECTED_REMOVE,
    payload: payload
})

export const toggleM = (id, cars, dispatch) => {
    const newRacingCars = [...cars];
    newRacingCars[id].buyCar = !newRacingCars[id].buyCar;
    newRacingCars[id].buyCar ? dispatch(updateSelectedAddAC()) : dispatch(updateSelectedSubAC());
    dispatch(updateCarAC(newRacingCars));
}

export default selectedReducer;