import ProductCard from "./ProductCard";
import PropTypes from 'prop-types';
import ProductCardBasket from "./ProductCardBasket";
import {useDispatch, useSelector} from "react-redux";
import {toggle} from "../store/modalReducer";
import Button from "./Button";
import {toggleM} from "../store/selectedReducer";

function MainPage({pageName}) {
    const dispatch = useDispatch();
    const racingCars = useSelector((state) => state.car.cars);

    let completedRacingCars = racingCars;
    if (pageName === 'Basket') {
        completedRacingCars = racingCars.filter(racingCar => racingCar.buyCar === true);
    } else if (pageName === 'Favorites') {
        completedRacingCars = racingCars.filter(racingCar => racingCar.completed === true);
    }

    const toggleModal = (show, id) => {
        const newModalState = !show;
        dispatch(toggle(id, newModalState));
    };

    const text = (buyCar, name) => {
        let text = "Are you sure you want to add the " + name + " to the cart?";

        if (buyCar === true) {
            text = "Have you already added " + name + " to your cart";
        }
        return text;
    }

    const modalActions = (show, id) => {
        return <div className="button-container">
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="Yes" onClick={() => {toggleModal(show, id); toggleM(id, racingCars, dispatch)}}/>
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="No" onClick={() => toggleModal(show, id)}/>
        </div>
    }

    const actions = (buyCar, show, id) => {
        if (buyCar === true) {
            return <div className="button-container">
                <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="Close"
                        onClick={() => toggleModal(show, id)}/>
            </div>

        } else {
            return <div className="button-container">
                <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="Yes" onClick={() => {toggleModal(show, id); toggleM(id, racingCars, dispatch)
                }}/>
                <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="No"
                        onClick={() => toggleModal(show, id)}/>
            </div>
        }
    }

    return (
        <>
            <ul className="car-list">
                {completedRacingCars.map((car) => (
                    pageName === 'Basket' ? <ProductCardBasket modalActions={modalActions} toggleModal={toggleModal} key={car.idCar} id={car.idCar} racingCar={car} /> :
                        <ProductCard text={text} actions={actions} toggleModal={toggleModal} key={car.idCar} id={car.idCar} racingCar={car} />
                ))}
            </ul>
            {!completedRacingCars.length && (<h1 className={"not-selected"}>You have not selected a product</h1>)}
        </>
    );
}

MainPage.propTypes = {
    pageName: PropTypes.string,
};

export default MainPage;