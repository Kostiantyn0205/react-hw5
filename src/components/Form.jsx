import {ErrorMessage, Field, Form, Formik} from "formik";
import validation from "../validation/validation";
import {useDispatch, useSelector} from "react-redux";
import {updateSelectedRemoveAC} from "../store/selectedReducer";
import PhoneInputMask from "./PhoneInput";
import React, {useEffect} from "react";
import {PatternFormat} from "react-number-format";

const FormikForm = () => {
    const dispatch = useDispatch();
    const racingCars = useSelector((state) => state.car.cars);
    let completedRacingCars = racingCars.filter(racingCar => racingCar.buyCar === true);

    const initialValues = {
        firstName: "",
        lastName: "",
        age: "",
        address: "",
        mobilePhone: "",
    }

    const handleSubmit = (values, {setSubmitting, resetForm}) => {
        console.log(values);
        console.log(completedRacingCars);

        racingCars.forEach((car) => {
            car.buyCar = false
        })
        dispatch(updateSelectedRemoveAC(0));

        resetForm(initialValues);
        setSubmitting(false);
    };

    const validationSchema = validation();

    return (
        <Formik initialValues={initialValues} onSubmit={handleSubmit} validationSchema={validationSchema}>
            {({isSubmitting}) => (
                <Form className="form">
                    <h2 className={"title-form"}>Place your order</h2>

                    <Field type="text" name="firstName" placeholder="User's first name"/>
                    <ErrorMessage name="firstName" className="error" component="div"/>

                    <Field type="text" name="lastName" placeholder="User's last name"/>
                    <ErrorMessage name="lastName" className="error" component="div"/>

                    <Field type="number" name="age" placeholder="Age of the user"/>
                    <ErrorMessage name="age" className="error" component="div"/>

                    <Field type="text" name="address" placeholder="Delivery addresses"/>
                    <ErrorMessage name="address" className="error" component="div"/>

                    {/*<Field type="text" name="mobilePhone" placeholder="Mobile phone"/>*/}
                    <Field name="mobilePhone">{({ field }) => <PatternFormat {...field} format="(###) ###-##-##" mask="_" id="mobilePhone" placeholder="(###) ###-##-##" />}</Field>
                    <ErrorMessage name="mobilePhone" className="error" component="div" />

                    <input type="submit" value="Checkout" disabled={isSubmitting || completedRacingCars.length === 0}/>
                </Form>
            )}
        </Formik>
    )
}

export default FormikForm;