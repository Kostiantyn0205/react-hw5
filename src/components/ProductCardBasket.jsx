import PropTypes from 'prop-types';
import Modal from "./Modal";
import IconStarCard from "../icon/IconStarCard";
import IconButtonClose from "../icon/IconButtonClose";
import {useDispatch, useSelector} from "react-redux";
import {toggleF} from "../store/favoritesReducer";

function ProductCardBasket({modalActions, toggleModal, id, racingCar}) {
    const {imageUrl, name, color, price, completed, } = racingCar;
    const show = useSelector((state) => state.modal.modals[id] || false);
    const dispatch = useDispatch();
    const racingCars = useSelector((state) => state.car.cars);
    const actions = modalActions(show, id);

    return (
        <li className="car-item-basket">
            <div className={"basket"}>
                <div className="img-container-basket">
                    <img className={"car-img-basket"} src={imageUrl} alt={name}/>
                </div>
                <div className="basket-text">
                    <h2 className="car-name">{name}</h2>
                    <p className="car-info">Цвет: {color}</p>
                    <p className="car-info">Цена: ${price.toLocaleString()}</p></div>
            </div>
            <button className={"button-basket-close"} onClick={() => toggleModal(show, id)}>
                <IconButtonClose />
            </button>
            <div onClick={() => {toggleF(id, racingCars, dispatch)}} className={`car-item-icon-basket ${completed && "completed"}`}>
                <IconStarCard/>
            </div>
            {show && (<Modal header="Confirmation" closeButton={true} onClick={() => toggleModal(show, id)} text={"Are you sure you want to remove " + name + " from your cart?"} actions={actions}/>)}
        </li>
    );
}

ProductCardBasket.propTypes = {
    id:PropTypes.number,
    racingCars: PropTypes.array,
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default ProductCardBasket;