import Button from "./Button";
import PropTypes from 'prop-types';
import Modal from "./Modal";
import IconStarCard from "../icon/IconStarCard";
import {useDispatch, useSelector} from "react-redux";
import {toggleF} from "../store/favoritesReducer";

function ProductCard({text, actions, toggleModal, id, racingCar}) {
    const {imageUrl, name, color, price, completed, buyCar} = racingCar;
    const show = useSelector((state) => state.modal.modals[id] || false);
    const dispatch = useDispatch();
    const racingCars = useSelector((state) => state.car.cars);
    const modalActions = actions(buyCar, show, id)
    const actionText = text(buyCar, name)

    return (
        <li className="car-item">
            <div className="img-container">
                <img className={"car-img"} src={imageUrl} alt={name}/>
            </div>
            <h2 className="car-name">{name}</h2>
            <p className="car-info">Цвет: {color}</p>
            <p className="car-info">Цена: ${price.toLocaleString()}</p>
            <Button text="Add to cart" onClick={() => toggleModal(show, id)}/>
            <div onClick={() => {toggleF(id, racingCars, dispatch)}} className={`car-item-icon ${completed && "completed"}`}>
                <IconStarCard/>
            </div>
            {show && (<Modal header="Confirmation" closeButton={true} onClick={() => toggleModal(show, id)} text={actionText} actions={modalActions}/>)}
        </li>
    );
}

ProductCard.propTypes = {
    id: PropTypes.number,
    racingCar: PropTypes.object,
    toggleCounterFavorites: PropTypes.func,
    toggleCounterSelected: PropTypes.func
};

export default ProductCard;