import PropTypes from 'prop-types';

const Button = ({backgroundColor, textColor, text, onClick}) => {
    const buttonStyle = {
        backgroundColor: backgroundColor,
        color: textColor,
    };

    return <button className="add-to-cart-button" style={buttonStyle} onClick={onClick}>{text}</button>;
}

Button.propTypes = {
    backgroundColor: PropTypes.string,
    textColor: PropTypes.string,
    onClick: PropTypes.func,
    text: PropTypes.string,
};

Button.defaultProps = {
    backgroundColor: "none",
    textColor: "none",
    text: "Button",
};

export default Button;