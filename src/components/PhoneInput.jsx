import React from 'react';
import { PatternFormat  } from 'react-number-format';

function PhoneInputMask() {

    return (
        <PatternFormat
            type="mobilePhone"
            format="+38(###)###-##-##"
            mask="_"
            allowEmptyFormatting={false}
            placeholder="+38(###)###-##-##"
        />
    );
}

export default PhoneInputMask;
